setwd(dir = "/Users/abinthomasonline/Desktop/jp")
train <- read.csv(file = 'train.csv', header = TRUE, sep = ',')
train_rows <- nrow(train)
test <- read.csv(file = 'test.csv', header = TRUE, sep = ',')
test_rows <- nrow(test)
dataset <- rbind(train[,1:8], test)

nrows <- nrow(dataset)

#name
dataset$d_male <- as.numeric(substr(dataset$Name, 1, 3)=='Mr.') + as.numeric(substr(dataset$Name, 1, 5)=='Dr. M')
dataset$d_dr <- as.numeric(substr(dataset$Name, 1, 1)=='D')
dataset$d_mr <- as.numeric(substr(dataset$Name, 1, 3)=='Mr.')
dataset$d_mrs <- as.numeric(substr(dataset$Name, 1, 3)=='Mrs')
dataset$d_miss <- as.numeric(substr(dataset$Name, 1, 2)=='Mi')

#date of birth
dataset$d_age <- as.numeric(as.Date(x = '2017-01-01') - as.Date(x = dataset$Date.of.Birth))

#from to
dataset$d_dist <- numeric(nrows)
from <- 'Chennai'
to <- 'Delhi'
dataset$d_dist[(dataset$From==from & dataset$To==to) | (dataset$From==to & dataset$To==from)] <- 1760
to <- 'Hyderabad'
dataset$d_dist[(dataset$From==from & dataset$To==to) | (dataset$From==to & dataset$To==from)] <- 633
to <- 'Kolkata'
dataset$d_dist[(dataset$From==from & dataset$To==to) | (dataset$From==to & dataset$To==from)] <- 1678
to <- 'Lucknow'
dataset$d_dist[(dataset$From==from & dataset$To==to) | (dataset$From==to & dataset$To==from)] <- 1534
to <- 'Mumbai'
dataset$d_dist[(dataset$From==from & dataset$To==to) | (dataset$From==to & dataset$To==from)] <- 1332
to <- 'Patna'
dataset$d_dist[(dataset$From==from & dataset$To==to) | (dataset$From==to & dataset$To==from)] <- 1490
from <- 'Delhi'
to <- 'Hyderabad'
dataset$d_dist[(dataset$From==from & dataset$To==to) | (dataset$From==to & dataset$To==from)] <- 1547
to <- 'Kolkata'
dataset$d_dist[(dataset$From==from & dataset$To==to) | (dataset$From==to & dataset$To==from)] <- 1307
to <- 'Lucknow'
dataset$d_dist[(dataset$From==from & dataset$To==to) | (dataset$From==to & dataset$To==from)] <- 417
to <- 'Mumbai'
dataset$d_dist[(dataset$From==from & dataset$To==to) | (dataset$From==to & dataset$To==from)] <- 1148
to <- 'Patna'
dataset$d_dist[(dataset$From==from & dataset$To==to) | (dataset$From==to & dataset$To==from)] <- 1054
from <- 'Hyderabad'
to <- 'Kolkata'
dataset$d_dist[(dataset$From==from & dataset$To==to) | (dataset$From==to & dataset$To==from)] <- 1505
to <- 'Lucknow'
dataset$d_dist[(dataset$From==from & dataset$To==to) | (dataset$From==to & dataset$To==from)] <- 1425
to <- 'Mumbai'
dataset$d_dist[(dataset$From==from & dataset$To==to) | (dataset$From==to & dataset$To==from)] <- 622
to <- 'Patna'
dataset$d_dist[(dataset$From==from & dataset$To==to) | (dataset$From==to & dataset$To==from)] <- 1140
from <- 'Kolkata'
to <- 'Lucknow'
dataset$d_dist[(dataset$From==from & dataset$To==to) | (dataset$From==to & dataset$To==from)] <- 886
to <- 'Mumbai'
dataset$d_dist[(dataset$From==from & dataset$To==to) | (dataset$From==to & dataset$To==from)] <- 1660
to <- 'Patna'
dataset$d_dist[(dataset$From==from & dataset$To==to) | (dataset$From==to & dataset$To==from)] <- 462
from <- 'Lucknow'
to <- 'Mumbai'
dataset$d_dist[(dataset$From==from & dataset$To==to) | (dataset$From==to & dataset$To==from)] <- 1190
to <- 'Patna'
dataset$d_dist[(dataset$From==from & dataset$To==to) | (dataset$From==to & dataset$To==from)] <- 447
from <- 'Mumbai'
dataset$d_dist[(dataset$From==from & dataset$To==to) | (dataset$From==to & dataset$To==from)] <- 1460

#flight date
flight_day <- weekdays.Date(as.Date(dataset$Flight.Date))
dataset$d_sun <- as.numeric(flight_day=='Sunday')
dataset$d_sat <- as.numeric(flight_day=='Saturday')
dataset$d_mon <- as.numeric(flight_day=='Monday')
dataset$d_fri <- as.numeric(flight_day=='Friday')
dataset$d_other_day <- as.numeric(flight_day=='Tuesday')+as.numeric(flight_day=='Wednesday')+as.numeric(flight_day=='Thursday')

#flight time
flight_time <- as.numeric(as.character(substr(dataset$Flight.Time, 1, 2)))
dataset$d_5_10 <- as.numeric(flight_time>=5 & flight_time<10)
dataset$d_10_17 <- as.numeric(flight_time>=10 & flight_time<17)
dataset$d_17_22 <- as.numeric(flight_time>=17 & flight_time<22)
dataset$d_22_5 <- as.numeric(flight_time>=22 & flight_time<5)

#flight date - booking date
dataset$d_adv_book <- as.numeric(as.Date(x = dataset$Flight.Date) - as.Date(x = dataset$Booking.Date))

#class
dataset$d_economy <- as.numeric(dataset$Class=='Economy')

features <- c(3, 4, 9:26)

train <- cbind(dataset[1:train_rows, features], train[, 9])
colnames(train)[21] <- 'Fare'
test <- dataset[(train_rows+1):nrows, features]

library(h2o)
h2o.init()

h2o_data <- as.h2o(train, destination_frame = 'data001')

h2o_split_data <- h2o.splitFrame(data = h2o_data, ratios = c(.7, .15), destination_frames = c('train', 'test', 'valid'))

rf_model <- h2o.randomForest(y = 21, training_frame = h2o_data, model_id = 'rf001', validation_frame = h2o_split_data[[3]], verbose = TRUE, score_each_iteration = TRUE, ntrees = 200)

prediction <- h2o.predict(object = rf_model, as.h2o(test))
submission <- as.data.frame(prediction)
paste(submission, collapse = ', ')
