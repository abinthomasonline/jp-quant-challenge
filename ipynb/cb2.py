import pandas as pd
import numpy as np
from catboost import CatBoostRegressor

train = pd.read_csv("d_train.csv")
test = pd.read_csv("d_test.csv")

X = train.drop(['Fare'], axis=1)
y = train.Fare

from sklearn.model_selection import train_test_split
X_train, X_validation, y_train, y_validation = train_test_split(X, y, train_size=0.8, random_state=1234)

print(X.dtypes)

#categorical_features_indices = np.where(X.dtypes != np.float)[0]
categorical_features_indices = np.array([0, 1, 2, 3, 4, 5, 6, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19])
print(categorical_features_indices)

from catboost import CatBoostRegressor
model=CatBoostRegressor(iterations=20000, depth=5, learning_rate=0.1, loss_function='RMSE', reg_lambda=0.001)
model.fit(X_train, y_train,cat_features=categorical_features_indices,eval_set=(X_validation, y_validation),plot=False)

submission = pd.DataFrame()
submission['Fare'] = model.predict(test)
submission.to_csv("Submission2.csv", index=False)

'''
import pandas as pd
sub = pd.read_csv('Submission.csv', header=None)
s = 'c('
for i in sub[0]:
    s = s + str(i) + ', '
s = s + ')'
s
'''